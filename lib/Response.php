<?php

class Response
{
    const DEFAULT_PARENT_VIEW = "__document";

    public $parent_view = "__document";

    public function redirect($url = null) {
        header("location: ".(preg_match("/^https?\:\/\//", $url) ? $url : url($url)));
    }

    public function findView($view) {
        foreach ([".html", ".htm", ".php"] as $ext) {
            $filepath = "views/".$view.$ext;

            if (file_exists($filepath)) {
                return $filepath;
            }
        }

        return false;
    }

    public function view($view, $params = []) {
        $filepath = $this->findView($view);

        if (!file_exists($filepath)) {
            return "View '$view' not found";
        }

        ob_start();

        // initiate view params
        $app = App::getInstance();

        if (count($params) > 0) {
            foreach ($params as $name => $val) {
                $$name = $val;
            }
        }

        include $filepath;

        return ob_get_clean();
    }

    public function error($code = 500, $message = null) {
        $default_message = [
            404 => "Page not found",
            500 => "Internal Server Error",
        ];

        $view = $this->findView("errors/$code") ? "errors/$code" : "errors/app";
        $params = [
            "code" => $code,
            "message" => $message === null ? $default_message[$code] : $message,
        ];

        if (DEBUG) {
            ob_start();
            trace();
            $params["backtrace"] = ob_get_clean();
        }

        return view($view, $params);
    }
}
