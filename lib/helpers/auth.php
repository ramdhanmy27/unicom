<?php

function redirect_if_guest() {
    $is_guest = !App::auth()->check();

    if ($is_guest) {
        App::session()->flash("You are not authenticated", "danger");
        redirect("auth/login");
    }

    return $is_guest;
}

function redirect_if_user() {
    if (App::auth()->check()) {
        return redirect("/");
    }
}
