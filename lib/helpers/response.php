<?php

function view($view, $params = []) {
    $response = App::response();
    $content = $response->view($view, $params);

    $view = $response->view(App::response()->parent_view, [
        "content" => $content,
    ]);

    // reset to default parent view
    App::response()->parent_view = Response::DEFAULT_PARENT_VIEW;

    return $view;
}

function redirect($url) {
    return App::response()->redirect($url);
}

function back() {
    if (isset($_SERVER["HTTP_REFERER"])) {
        $url = $_SERVER["HTTP_REFERER"];
    }

    redirect($url);
}

function abort($code, $message = null) {
    return App::response()->error($code, $message);
}

function parent_view($view) {
    App::response()->parent_view = $view;
}
