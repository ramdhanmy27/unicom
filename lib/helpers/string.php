<?php

function file_extension($filename) {
    return substr($filename, strrpos($filename, '.'));
}

function filter($value) {
    return addslashes(htmlspecialchars($value));
    return filter_var($value, FILTER_SANITIZE_STRING);
}

function studly_case($value) {
    return str_replace(" ", "", ucwords(preg_replace("/[\-_]/", " ", $value)));
}

function camel_case($value) {
    return lcfirst(studly_case($value));
}

function snake_case($value) {
    return strtolower(preg_replace(["/[A-Z]/", "/^\s+/", "/ /"], [" $0", "", "_"], $value));
}

function hyphen_case($value) {
    return str_replace("_", "-", snake_case($value));
}

function money($value, $currency = "Rp", $decimals = 0) {
    return ($currency !== null ? $currency." " : "").number_format($value, $decimals, ",", ".");
}

function truncate($string, $limit = 100) {
    if (strlen($string) > $limit) {
        return substr($string, 0, $limit)."...";
    }

    return $string;
}