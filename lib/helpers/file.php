<?php

function get_extension($filename) {
    return substr($filename, strrpos($filename, ".") + 1);
}

function has_extension($filename, array $extensions) {
    return in_array(strtolower(get_extension($filename)), $extensions);
}

function is_image($filename) {
    return has_extension($filename, ["png", "jpg", "jpeg", "gif"]);
}
