<?php

function url($path, $query = []) {
    $query_params = count($query) > 0 ? "?".http_build_query($query) : "";
	return clean_url(App::request()->origin()."/".$path).$query_params;
}

function clean_url($url) {
    return preg_replace(["/\\\\/", "/([^:])\/+/"], ["/", "$1/"], $url);
}
