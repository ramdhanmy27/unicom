<?php

function product_router($data) {
    return url("product/show/".seo_url($data["name"]."-".$data['id']));
}

function category_router($data) {
    return url("product/category/".seo_url($data["name"]."-".$data['id']));
}

function seo_url($url) {
    return urlencode(hyphen_case(preg_replace("/[^\w\-]/", "-", $url)));
}
