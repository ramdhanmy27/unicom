<?php

function array_only($array, $keys, $callback = null) {
    $results = [];

    foreach ($keys as $key) {
        $results[$key] = is_callable($callback) ? $callback($array[$key]) : $array[$key];
    }

    return $results;
}
