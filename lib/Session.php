<?php

class Session
{
    private $flash_types = ["info", "success", "primary", "danger"];

    public function set($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function get($key, $default = null) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
    }

    public function remove($key) {
        if ($this->has($key)) {
            unset($_SESSION[$key]);
        }
    }

    public function has($key) {
        return isset($_SESSION[$key]);
    }

    public function push($key, $message) {
        if (!isset($_SESSION[$key])) {
	        $_SESSION[$key] = [];
    	}

    	if (is_array($_SESSION[$key])) {
	        $_SESSION[$key][] = $message;
    	}
    }

    public function pull($key) {
        if (isset($_SESSION[$key]) && is_array($_SESSION[$key]) && count($_SESSION[$key]) > 0) {
	        $values = $_SESSION[$key];
	        $_SESSION[$key] = [];

	        return $values;
    	}
    }

    public function pop($key) {
        if (isset($_SESSION[$key]) && is_array($_SESSION[$key]) && count($_SESSION[$key]) > 0) {
	        return array_pop($_SESSION[$key]);
    	}
    }

    public function flash($message, $type = "info") {
    	$this->push("flash.$type", $message);
    }

    public function getFlash($type = null) {
        if ($type === null) {
            $messages = [];

            foreach ($this->flash_types as $flash_type) {
                $key = "flash.$flash_type";

                if ($this->has($key)) {
                    $messages[$flash_type] = $this->pull($key);
                }
            }

            return $messages;
        }

    	return $this->pull("flash.$type");
    }
}
