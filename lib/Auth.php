<?php

class Auth
{
	public function signin($username, $password) {
		$user = App::db()->find("users", [
			"email" => $username,
			"password" => $this->hash($password),
		]);

		// set login session
		if ($user) {
			App::session()->set("user_id", $user["id"]);
			App::session()->set("is_staff", $user["is_staff"]);
		}

		return $user;
	}

	public function logout() {
		App::session()->remove("user_id");
		App::session()->remove("is_staff");
	}

	public function register($data) {
		$data["password"] = $this->hash($data["password"]);
		$data["is_staff"] = 0;

		App::db()->insert("users", $data);
	}

	public function user() {
		return App::db()->find("users", App::session()->get("user_id"));
	}

	public function check() {
		return App::session()->has("user_id");
	}

	public function isStaff() {
		return App::session()->get("is_staff");
	}

	public function hash($value) {
		return md5($value);
	}
}
