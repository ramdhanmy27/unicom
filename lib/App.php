<?php

session_name(SITE_KEY);
session_start();

class App
{
	private static $instance;

	private function __construct() {}

	public static function init()
	{
		self::$instance = new App();

		// load libraries & helpers
		self::$instance->loadHelpers("lib/helpers/*.php");
		self::$instance->loadLibrary([
			"Db",
			"Session",
			"Request",
			"Response",
			"Auth",
		]);

		// executing controllers through url path
		echo self::$instance->loadController();
	}

	public static function getInstance() {
		return self::$instance;
	}

	public static function __callStatic($method, $args) {
		return self::$instance->{$method};
	}

	private function loadLibrary($libraries)
	{
		foreach ($libraries as $library) {
			include "lib/$library.php";

			if (class_exists($library)) {
				$instance_name = snake_case($library);
				$this->{$instance_name} = new $library;
			}
		}
	}

	private function loadHelpers($path)
	{
		$helpers = glob($path);

		foreach ($helpers as $helper) {
			include $helper;
		}
	}

	private function loadController()
	{
		$path = $this->request->path();
		$this->checkMiddleware($path);

		// Find Controller File
		$controller_file = $this->findController($path);

		if ($controller_file === false) {
			return $this->response->error(404);
		}

		// Load Controller Class
		require $controller_file;
		$controller_class = basename(substr($controller_file, 0, strrpos($controller_file, ".")));

		if (class_exists($controller_class)) {
			$this->controller = new $controller_class;

			// default action
			$action_method = "index";
			$action_params = [];

			// remove app/Controllers
			$file_params = array_slice(explode("/", $controller_file), 2);
			$file_params_count = count($file_params);
			$url_params = explode("/", $path);

			if (count($url_params) > $file_params_count) {
				$action_method = camel_case(array_slice($url_params, $file_params_count, 1)[0]);
				$action_params = array_slice($url_params, $file_params_count + 1);
			}

			// check if the method is accessible
			if (method_exists($controller_class, $action_method)) {
				$r = new ReflectionMethod($controller_class, $action_method);
				$method_params = $r->getParameters();

				foreach ($method_params as $i => $method_param) {
					if (!$method_param->isOptional() && !isset($action_params[$i])) {
						return $this->response->error(404);
					}
				}

				return call_user_func_array([$this->controller, $action_method], $action_params);
			}

			return $this->response->error(404);
		}
		else {
			return $this->response->error(500, "Class '$controller_class' not found.");
		}
	}

	private function findController($path) {
		$controller_path = "app/Controllers";

		// default controller
		if ($path == null) {
			return $controller_path."/IndexController.php";
		}

		$url_params = explode("/", $path);
		$tmp_path = $controller_path;

		// Load Controller by url
		foreach ($url_params as $param) {
			// check file controller
			// **/AppController.php
			$controller_file = $tmp_path."/".studly_case($param)."Controller.php";

			if (file_exists($controller_file) && is_file($controller_file)) {
				return $controller_file;
			}

			// // **/app/IndexController.php
			// $controller_file = $tmp_path."/$param/IndexController.php";

			// if (file_exists($controller_file) && is_file($controller_file)) {
			// 	return $controller_file;
			// }

			$tmp_path .= "/".$param;
		}

		// Load Index Controller as fallback
		$tmp_path = $controller_path;

		foreach ($url_params as $param) {
			// check file controller
			$controller_file = $tmp_path."/IndexController.php";

			if (file_exists($controller_file) && is_file($controller_file)) {
				return $controller_file;
			}

			$tmp_path .= "/".$param;
		}

		return false;
	}

	public function checkMiddleware($path) {
		if (preg_match("/^admin/", $path)) {
			if (!App::auth()->isStaff()) {
				App::session()->flash("You are not authenticated", "danger");

				return redirect("auth/login");
			}
		}
	}
}
