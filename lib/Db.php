<?php

class Db extends PDO
{
	public function __construct($host=null, $port=null, $user=null, $pass=null, $dbname=null)
	{
		$host = $host ? $host : DB_HOST;
		$port = $port ? $port : DB_PORT;
		$user = $user ? $user : DB_USER;
		$pass = $pass ? $pass : DB_PWD;
		$dbname = $dbname ? $dbname : DB_NAME;

		parent::__construct('mysql:host='.$host.';port='.$port.';dbname='.$dbname, $user, $pass);
		$this->setAttribute(PDO::ATTR_ERRMODE, DEBUG ? PDO::ERRMODE_EXCEPTION : PDO::ERRMODE_SILENT);
	}

	public function find($table, $where = null) {
		return $this->findQuery($table, $where)->fetch();
	}

	public function findAll($table, $where = null) {
		return $this->findQuery($table, $where)->fetchAll(PDO::FETCH_ASSOC);
	}

	public function findQuery($table, $where = null) {
		$where_clause = [];
		$params = [];
		$sql = "SELECT * FROM `$table`";

		if ($where) {
			if (!is_array($where)) {
				$where = ["id" => $where];
			}

			foreach ($where as $field => $value) {
				if (is_array($value)) {
					$where_clause[] = $field." ".current($value)." ?";
					$params[] = next($value);
				}
				else {
					$where_clause[] = "$field=?";
					$params[] = $value;
				}
			}

			$sql .= " WHERE ".implode(" AND ", $where_clause);
		}

		return $this->query($sql, $params);
	}

	public function query($query, $params = []) {
		$pdo = $this->prepare($query);
		$pdo->execute($params);

		return $pdo;
	}

	public function select($query, $params = [], $fetchMode = null) {
		$pdo = $this->prepare($query);
		$pdo->setFetchMode($fetchMode ? $fetchMode : PDO::FETCH_ASSOC);

		if ($pdo->execute($params)) {
			return $pdo->fetchAll();
		}
	}

	public function insert($table, $data) {
		$data_keys = array_keys($data);
		$is_multiple_values = $data_keys == range(0, count($data_keys) - 1);

		if (!$is_multiple_values) {
			$fields = $data_keys;
			$data = [$data];
		}

		$sql_values = [];
		$params = [];

		foreach ($data as $values) {
			if (!isset($fields)) {
				$fields = array_keys($fields);
			}

			$sql_values[] = "(".implode(",", array_fill(0, count($values), "?")).")";
			$params = array_merge($params, array_values($values));
		}

		$sql = "INSERT INTO `$table` (`".implode("`,`", $fields)."`) VALUES ".implode(",", $sql_values);

		return $this->query($sql, $params);
	}

	public function update($table, $data, $where = null) {
		$sql = "UPDATE `$table` ";
		$params = [];

		// UPDATE Field
		$sets = [];

		foreach ($data as $field => $value) {
			$sets[] = "`$field`=?";
			$params[] = $value;
		}

		$sql .= " SET ".implode(",", $sets);

		// UPDATE Where Clause
		if ($where) {
			$where_clause = [];

			if (!is_array($where)) {
				$where = ["id" => $where];
			}

			foreach ($where as $field => $value) {
				if (is_array($value)) {
					$where_clause[] = $field." ".current($value)." ?";
					$params[] = next($value);
				}
				else {
					$where_clause[] = "$field=?";
					$params[] = $value;
				}
			}

			$sql .= " WHERE ".implode(" AND ", $where_clause);
		}

		return $this->query($sql, $params);
	}
}
