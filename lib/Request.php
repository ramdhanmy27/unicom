<?php

class Request
{
    public function __get($name) {
        return $this->input($name);
    }

    public function isPost() {
        return count($_POST) > 0;
    }

    public function isGet() {
        return count($_POST) === 0;
    }

    public function has($key) {
        return isset($_REQUEST[$key]);
    }

    public function hasGet($key) {
        return isset($_GET[$key]);
    }

    public function hasPost($key) {
        return isset($_POST[$key]);
    }

    public function hasFile($key) {
        return isset($_FILES[$key]);
    }

    public function input($key, $default = null) {
        return $this->has($key) ? filter($_REQUEST[$key]) : $default;
    }

	function get($key, $default = null) {
        return $this->hasGet($key) ? filter($_GET[$key]) : $default;
	}

    function post($key, $default = null) {
        return $this->hasPost($key) ? filter($_POST[$key]) : $default;
    }

	function file($key, $default = null) {
        return $this->hasFile($key) ? $_FILES[$key] : $default;
	}

    public function only($keys) {
        return array_only($_REQUEST, $keys, function($value) {
            return filter($value);
        });
    }

    public function all($key) {
        $data = [];

        foreach ($_REQUEST as $key => $value) {
            $data[$key] = filter($value);
        }

        return $data;
    }

    public function path($index=null) {
        if (!isset($_GET['url'])){
            return null;
        }

        return preg_replace("/[\/\\\\]+$/", "", $_GET['url']);
    }

    public function origin() {
        $scheme = isset($_SERVER["REQUEST_SCHEME"]) ? $_SERVER["REQUEST_SCHEME"] : "http";
        return $scheme."://".$_SERVER["HTTP_HOST"].dirname($_SERVER["PHP_SELF"]);
    }

    public function url() {
        return $this->origin()."/".$this->path();
    }
}