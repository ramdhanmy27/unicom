<?php

  $attributes = null;

  if (isset($data["attributes"])) {
    $attributes = json_decode($data["attributes"]);
  }

?>

<form method="post" enctype="multipart/form-data">
  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* <?php echo $fields["name"]; ?></div>
    <div class="col-sm-7">
      <input type="text" name="name" class="form-control" value="<?php echo @$data['name']; ?>" required />
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label"><?php echo $fields["parent_id"]; ?></div>
    <div class="col-sm-7">
      <select name="parent_id" class="form-control" required autocomplete="off">
        <option value="">-- Pilih Kategori --</option>
        <?php foreach (App::db()->select("SELECT * FROM category order by name") as $opt): ?>
          <option
            value="<?php echo $opt["id"]; ?>"
            <?php echo $opt["id"]==@$data["parent_id"] ? 'selected="selected"' : ""; ?>
          >
            <?php echo $opt["name"]; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label"></div>
    <div class="col-sm-7">
      <button type="submit" class="btn btn-primary">Simpan</button>
      <input type="reset" value="Reset" class="btn" />
    </div>
  </div>
</form>
