<?php parent_view("admin/layout"); ?>

<div class="table-responsive">
  <div class="table-responsive mt-3">
    <?php if (count($data) > 0): ?>
      <table class="table">
        <thead>
          <tr>
            <th> Waktu Pesan </th>
            <th> Pemesan </th>
            <th> Alamat </th>
            <th> Carrier </th>
            <th> Total </th>
            <th> Status </th>
            <th> </th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($data as $order) : ?>
            <tr>
              <td> <?php echo date("d/m/Y H:i:s", strtotime($order["order_time"])); ?> </td>
              <td> 
                <b><?php echo $order["name"]; ?> </b>
                <br /> <span class="text-muted"><?php echo $order["phone"]; ?></span>
              </td>
              <td> 
                <?php echo $order["address"]; ?>, <?php echo $order["postal_code"]; ?>, 
                <br /> <?php echo $order["city"]; ?>, <?php echo $order["province"]; ?>,
              </td>
              <td> <?php echo $order["carrier"]; ?> </td>
              <td> <?php echo money($order["total"]); ?> </td>
              <td> 
                <?php if ($order["status"] == '1'): ?>
                  <span class="badge badge-secondary">Menunggu</span>
                <?php endif; ?>
                <?php if ($order["status"] == '2'): ?>
                  <span class="badge badge-success">Terkirim</span>
                <?php endif; ?>
              </td>
              <td>
                <?php if ($order["status"] == '1'): ?>
                  <a 
                    href="<?php echo url("admin/orders/sent/$order[id]"); ?>" 
                    class="btn btn-sm btn-danger"
                    onClick="return confirm('Apakah anda yakin ?');"
                  >
                    <i class="fa fa-truck"></i> Kirim
                  </a>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h1 class="p-5 text-center text-muted">Empty</h1>
    <?php endif; ?>
  </div>
</div>