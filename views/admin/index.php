<?php parent_view("admin/layout"); ?>
<h2><?php echo $message; ?></h2><hr />

<div class="col-md-10 offset-md-1 col-12 pb-4 mt-3">
  <h4>Top 10 products</h4>

  <div class="row bg-white border p-2">
    <?php foreach ($products as $product): ?>
      <?php $attrs = json_decode($product["attributes"]); ?>

      <div class="product col-3 p-0 card">
        <div class="card-body p-2">
          <div class="position-relative">
            <a href="<?php echo product_router($product); ?>">
              <img class="card-img-top" src="<?php echo url($attrs->image); ?>">
            </a>
          </div>

          <div class="cart-text">
            <h5 class="text-truncate">
              <?php echo $product["name"]; ?> 
              <b>(<?php echo $product["count"]; ?>)</b>
            </h5>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>


<div class="col-md-10 offset-md-1 col-12 pb-4 mt-3">
  <div class="row bg-white border p-2">
    <h3>Jumlah Penjualan Bulan <?php echo date('F'); ?>: <?php echo $sell_count; ?></h3>
  </div>
</div>