<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Admin UNICOM</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut-icon" href="<?php echo url("assets/img/favicon.ico"); ?>">
    <link rel="stylesheet" href="<?php echo url("assets/css/bootstrap.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo url("assets/css/fontawesome-all.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo url("assets/css/style.css"); ?>">
  </head>
  <body>
    <?php $user = App::auth()->user(); ?>

    <!-- Header -->
    <nav id="header" class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
      <div class="col-md-3 col-5 text-center">
        <a class="navbar-brand" href="<?php echo url("admin/index") ?>">
          <img
            src="<?php echo url("assets/img/icon.jpg"); ?>"
            class="d-inline-block align-top"
            height="30"
            width="30"
          />
          <span class="text-muted">Admin</span> UNICOM
        </a>
      </div>

      <div class="col-md-6 col-5">
      </div>

      <div class="col-md-3 col-5">
        <ul class="navbar-nav flex-row">
          <li class="nav-item">
            <a class="nav-link px-2" href="<?php echo url("/"); ?>">My Site</a>
          </li>
          <li class="nav-item">
            <a class="nav-link px-2" href="<?php echo url("auth/profile"); ?>">Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link px-2" href="<?php echo url("auth/logout"); ?>">Logout</a>
          </li>
        </ul>
      </div>
    </nav>

    <!-- Main Container -->
    <div id="main-container" class="container mt-3">
      <?php
        $flashMessages = App::session()->getFlash();

        foreach ($flashMessages as $type => $messages) {
          if (!is_array($messages)) {
            continue;
          }

          foreach ($messages as $message) {
      ?>

            <div class="alert alert-<?php echo $type; ?> alert-dismissible fade show">
              <?php
                switch ($type) {
                  case 'danger': echo "<i class='fa pr-2 fa-times'></i>"; break;
                  case 'info': echo "<i class='fa pr-2 fa-info'></i>"; break;
                  case 'success': echo "<i class='fa pr-2 fa-check'></i>"; break;
                }

                echo $message;
              ?>

              <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

      <?php
          }
        }
      ?>

      <!-- Sidebar -->
      <?php
        $reqpath = App::request()->path();
        $menu = [
          "admin/index" => [
            "icon" => "fa fa-tachometer-alt",
            "label" => "Dashboard",
          ],
          "admin/user" => [
            "icon" => "fa fa-users",
            "label" => "User",
          ],
          "admin/product" => [
            "icon" => "fa fa-box",
            "label" => "Product",
          ],
          "admin/category" => [
            "icon" => "fa fa-th",
            "label" => "Category",
          ],
          "admin/orders" => [
            "icon" => "fa fa-shopping-basket",
            "label" => "Orders",
          ],
          "admin/user" => [
            "icon" => "fa fa-users",
            "label" => "Users",
          ],
        ];
      ?>

      <div class="row">
        <div class="col-3 d-none d-md-block">
          <div id="menu" class="border bg-white p-2">
            <ul class="nav nav-pills flex-column">
              <?php foreach ($menu as $urlpath => $item): ?>
                <li class="nav-item">
                  <a
                    href="<?php echo url($urlpath); ?>"
                    class="nav-link text-truncate <?php echo $urlpath == $reqpath ? "active" : "" ?>"
                  >
                    <i class="<?php echo $item["icon"]; ?>"></i>
                    <small><?php echo $item["label"]; ?></small>
                  </a>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>

        <!-- Content -->
        <div class="col-md-9 col-12 pb-4">
          <div class="card">
            <div class="card-body">
              <?php echo $content; ?>
            </div>
          </div>
        </div>
      </div>

    </div>

    <script type="text/javascript" src="<?php echo url("assets/js/jquery-3.2.1.slim.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo url("assets/js/bootstrap.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo url("assets/js/app.js"); ?>"></script>
  </body>
</html>
