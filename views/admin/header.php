<?php $path = App::request()->path(); ?>

<ul class="nav nav-pills">
    <li class="nav-item">
        <a
            href="<?php echo url("admin/product"); ?>"
            class="nav-link <?php echo $path=="admin/product" ? "active" : ""; ?>"
        >
            Product
        </a>
    </li>
    <li class="nav-item">
        <a
            href="<?php echo url("admin/user"); ?>"
            class="nav-link <?php echo $path=="admin/user" ? "active" : ""; ?>"
        >
            User
        </a>
    </li>
    <li class="nav-item">
        <a
            href="<?php echo url("admin/category"); ?>"
            class="nav-link <?php echo $path=="admin/category" ? "active" : ""; ?>"
        >
            Category
        </a>
    </li>
    <li class="nav-item">
        <a
            href="<?php echo url("admin/orders"); ?>"
            class="nav-link <?php echo $path=="admin/orders" ? "active" : ""; ?>"
        >
            Orders
        </a>
    </li>
</ul>
<hr />
