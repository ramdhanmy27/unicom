<?php

  $attributes = null;

  if (isset($data["attributes"])) {
    $attributes = json_decode($data["attributes"]);
  }

?>

<form method="post" enctype="multipart/form-data">
  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* <?php echo $fields["name"]; ?></div>
    <div class="col-sm-7">
      <input type="text" name="name" class="form-control" value="<?php echo @$data['name']; ?>" required />
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* <?php echo $fields["email"]; ?></div>
    <div class="col-sm-7">
      <input type="text" name="email" class="form-control" value="<?php echo @$data['email']; ?>" required />
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* Password</div>
    <div class="col-sm-7">
      <input type="password" name="password" class="form-control" />
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* <?php echo $fields["phone"]; ?></div>
    <div class="col-sm-7">
      <input type="text" name="phone" class="form-control" value="<?php echo @$data['phone']; ?>" required />
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 col-form-label text-right">
      Jenis Kelamin
    </label>
    <div class="col-md-7">
      <label class="col-form-label mr-2">
        <input type="radio" name="gender" value="L" required <?php echo @$data["gender"] == 'L' ? "checked" : ""; ?> /> Pria
      </label>
      <label class="col-form-label mr-2">
        <input type="radio" name="gender" value="P" required <?php echo @$data["gender"] == 'P' ? "checked" : ""; ?> /> Wanita
      </label>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 col-form-label text-right">
      Tanggal Lahir
    </label>
    <div class="col-md-7">
      <input type="date" class="form-control" name="date_birth" value="<?php echo @$data["date_birth"]; ?>" required />
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label"></div>
    <div class="col-sm-7">
      <button type="submit" class="btn btn-primary">Simpan</button>
      <input type="reset" value="Reset" class="btn" />
    </div>
  </div>
</form>
