<?php parent_view("admin/layout"); ?>
<?php

unset($fields["attributes"]);
unset($fields["discount"]);
unset($fields["stock"]);
unset($fields["weight"]);

?>

<div class="table-responsive">
  <a href="<?php echo url("admin/product/create"); ?>" class="btn btn-info">+ Create</a>

  <div class="table-responsive mt-3">
    <?php if (count($results) > 0): ?>
      <table class="table">
        <thead>
          <?php foreach ($fields as $label): ?>
            <th> <?php echo ucwords($label); ?> </th>
          <?php endforeach; ?>
        </thead>
        <tbody>
          <?php foreach ($results as $data) : ?>
            <tr>
              <?php foreach ($fields as $field => $label) : ?>
                <td>
                  <?php
                    switch ($field) {
                      case "discount":
                      case "price":
                        echo money($data[$field]);
                        break;

                      case "stock":
                        echo money($data[$field], null);
                        break;

                      case "weight":
                        echo money($data[$field], null)." gr";
                        break;

                      case "description":
                        echo truncate($data[$field]);
                        break;

                      default:
                        echo $data[$field];
                        break;
                    }
                  ?>
                </td>
              <?php endforeach; ?>
              <td>
                <a href="<?php echo url("admin/product/update/$data[id]"); ?>" class="btn btn-sm btn-warning">
                  <i class="fa fa-edit"></i>
                </a>
                <a
                  href="<?php echo url("admin/product/delete/$data[id]"); ?>"
                  class="btn btn-sm btn-danger"
                  onClick="return confirm('Apakah anda yakin ingin menghapus ?');"
                >
                  <i class="fa fa-trash"></i>
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h1 class="p-5 text-center text-muted">Empty</h1>
    <?php endif; ?>
  </div>
</div>