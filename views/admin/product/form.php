<?php

  $attributes = null;

  if (isset($data["attributes"])) {
    $attributes = json_decode($data["attributes"]);
  }

?>

<form method="post" enctype="multipart/form-data">
  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* <?php echo $fields["name"]; ?></div>
    <div class="col-sm-7">
      <input type="text" name="name" class="form-control" value="<?php echo @$data['name']; ?>" required />
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* <?php echo $fields["category_id"]; ?></div>
    <div class="col-sm-7">
      <select name="category_id" class="form-control" required autocomplete="off">
        <option value="">-- Pilih Kategori --</option>
        <?php foreach (App::db()->select("SELECT * FROM category order by name") as $opt): ?>
          <option
            value="<?php echo $opt["id"]; ?>"
            <?php echo $opt["id"]==@$data["category_id"] ? 'selected="selected"' : ""; ?>
          >
            <?php echo $opt["name"]; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* Gambar</div>
    <div class="col-sm-7">
      <input type="file" class="form-control" name="img" required />
      <div><small class="text-muted">*.jpg, .jpeg, .png</small></div>

      <?php if ($attributes): ?>
        <div class="img-thumbnail mt-2" style="display: inline-block;">
          <img src="<?php echo url($attributes->image); ?>" height="200" />
        </div>
      <?php endif; ?>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* <?php echo $fields["weight"]; ?></div>
    <div class="col-sm-7">
      <input type="number" class="form-control" name="weight" value='<?php echo @$data['weight']; ?>' required />
      <small class="text-muted">*per gram</small>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* <?php echo $fields["stock"]; ?></div>
    <div class="col-sm-7">
      <input type="number" class="form-control" name="stock" value="<?php echo @$data['stock']; ?>" required />
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label">* <?php echo $fields["price"]; ?></div>
    <div class="col-sm-7">
      <input type="number" class="form-control" name="price" value="<?php echo @$data['price']; ?>" required />
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label"><?php echo $fields["discount"]; ?></div>
    <div class="col-sm-7">
      <input type="number" class="form-control" name="discount" value="<?php echo @$data['discount']; ?>" />
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label"><?php echo $fields["description"]; ?></div>
    <div class="col-sm-7">
      <textarea class="form-control" name="description" rows="5"><?php echo @$data['description']; ?></textarea>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-3 text-right col-form-label"></div>
    <div class="col-sm-7">
      <button type="submit" class="btn btn-primary">Simpan</button>
      <input type="reset" value="Reset" class="btn" />
    </div>
  </div>
</form>
