<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>UNICOM</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut-icon" href="<?php echo url("assets/img/favicon.ico"); ?>">
    <link rel="stylesheet" href="<?php echo url("assets/css/bootstrap.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo url("assets/css/fontawesome-all.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo url("assets/css/style.css"); ?>">
  </head>
  <body>
    <!-- Header -->
    <nav id="header" class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
      <div class="col-md-3 col-5 text-center">
        <a class="navbar-brand" href="<?php echo url("/") ?>">
          <img
            src="<?php echo url("assets/img/icon.jpg"); ?>"
            class="d-inline-block align-top"
            height="30"
            width="30"
          />
          UNICOM
        </a>
      </div>

      <div class="col-md-5 col-4 d-none d-md-block">
        <form
          class="form-inline row d-block"
          <?php
            $path = App::request()->path();

            if (!preg_match("/^product/", $path) || preg_match("/^product\/show\//", $path)) {
              echo "action='".url("product")."'";
            }
          ?>
        >
          <div class="input-group">
            <input
              class="form-control"
              type="text"
              name="q"
              placeholder="Search"
              value="<?php echo App::request()->get("q"); ?>"
            >

            <div class="input-group-append">
              <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
            </div>
          </div>
        </form>
      </div>

      <div class="col-md-3 col-5">
        <ul class="navbar-nav flex-row">
          <li class="nav-item">
            <a class="nav-link px-2" href="<?php echo url("product"); ?>">
              <b>Telusuri Barang</b>
            </a>
          </li>

          <?php if (!App::auth()->check()): ?>
            <li class="nav-item">
              <a class="nav-link px-2" href="<?php echo url("auth/register"); ?>">Register</a>
            </li>
            <li class="nav-item">
              <a class="nav-link px-2" href="<?php echo url("auth/login"); ?>">Login</a>
            </li>
          <?php else: ?>
            <?php $user = App::auth()->user(); ?>

            <li class="nav-item">
              <a class="nav-link px-2" href="<?php echo url("auth/profile"); ?>">
                <?php echo $user["name"]; ?>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link px-2" href="<?php echo url("auth/logout"); ?>">Logout</a>
            </li>
          <?php endif; ?>
        </ul>
      </div>

      <div class="col-md-1 col-2 text-center">
        <a class="btn" href="<?php echo url("cart"); ?>">
          <i class="fas fa-shopping-cart"></i>
        </a>
      </div>
    </nav>

    <!-- Main Container -->
    <div id="main-container" class="container mt-3">
      <?php
        $flashMessages = App::session()->getFlash();

        foreach ($flashMessages as $type => $messages) {
          if (!is_array($messages)) {
            continue;
          }

          foreach ($messages as $message) {
      ?>

            <div class="alert alert-<?php echo $type; ?> alert-dismissible fade show">
              <?php
                switch ($type) {
                  case 'danger': echo "<i class='fa pr-2 fa-times'></i>"; break;
                  case 'info': echo "<i class='fa pr-2 fa-info'></i>"; break;
                  case 'success': echo "<i class='fa pr-2 fa-check'></i>"; break;
                }

                echo $message;
              ?>

              <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

      <?php
          }
        }
      ?>

      <?php echo $content; ?>
    </div>

    <script type="text/javascript" src="<?php echo url("assets/js/jquery-3.2.1.slim.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo url("assets/js/bootstrap.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo url("assets/js/app.js"); ?>"></script>
  </body>
</html>
