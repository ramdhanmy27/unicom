<!-- Main Container -->
<div class="container mt-3">
  <?php
    $flashMessages = App::session()->getFlash();

    foreach ($flashMessages as $type => $messages) {
      if (!is_array($messages)) {
        continue;
      }

      foreach ($messages as $message) {
  ?>

        <div class="alert alert-<?php echo $type; ?> alert-dismissible fade show">
          <?php
            switch ($type) {
              case 'danger': echo "<i class='fa pr-2 fa-times'></i>"; break;
              case 'info': echo "<i class='fa pr-2 fa-info'></i>"; break;
              case 'success': echo "<i class='fa pr-2 fa-check'></i>"; break;
            }

            echo $message;
          ?>

          <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

  <?php
      }
    }
  ?>
</div>

<!-- Main Container -->
<form class="needs-validation" method="post" action="<?php echo url("cart/checkout");?>">
  <div id="main-container" class="container-fluid mt-3">
    <div class="row px-4">
      <div class="col-md-6 offset-1 border bg-white mb-3 py-3">
        <h5 class="pb-3 mb-3 border-bottom text-muted">Delivery Detail</h5>

        <div class="detail mt-4 text">
          <div class="mb-3">
            <label for="name">Name</label>
            <div class="input-group">
              <input type="hidden" name="user_id" value="<?php echo $user["id"] ?>">
              <input type="text" class="form-control" name="name" placeholder="Name" required value="<?php echo $user["name"] ?>">
              <div class="invalid-feedback" style="width: 100%;">
                Your username is required.
              </div>
            </div>
          </div>

          <div class="mb-3">
            <label for="phone">Phone</label>
            <input type="text" class="form-control" name="phone" value="<?php echo $user["phone"] ?>">
            <div class="invalid-feedback">
              Please enter a valid phone number.
            </div>
          </div>

          <div class="mb-3">
            <label for="address">Address</label>
            <input type="text" class="form-control" name="address" placeholder="address.." required=""
              value="<?php echo $order["address"] ?>">
            <div class="invalid-feedback">
              Please enter your shipping address.
            </div>
          </div>

          <div class="mb-3">
            <label for="city">City</label>
            <input type="text" class="form-control" name="city" placeholder="city.." required=""
              value="<?php echo $order["city"] ?>">
            <div class="invalid-feedback">
              Please enter your city shipping address.
            </div>
          </div>

          <div class="mb-3">
            <label for="province">Province</label>
            <input type="text" class="form-control" name="province" placeholder="province.." required=""
              value="<?php echo $order["province"] ?>">
            <div class="invalid-feedback">
              Please enter your province shipping address.
            </div>
          </div>

          <div class="mb-3">
            <label for="address2">Postal Code</label>
            <input type="text" class="form-control" name="postal_code" placeholder="postal code.." value="<?php echo $order["postal_code"]; ?>">
          </div>

          <h4 class="mb-3">Carrier</h4>

          <div class="d-block my-3">
            <div class="custom-control custom-radio">
              <input name="carrier" value="dhl" type="radio" class="custom-control-input" checked="" required="">
              <label class="custom-control-label" for="dhl">DHL</label>
            </div>
            <div class="custom-control custom-radio">
              <input name="carrier" value="fedex" type="radio" class="custom-control-input" required="">
              <label class="custom-control-label" for="fedex">FedEx</label>
            </div>
            <div class="custom-control custom-radio">
              <input name="carrier" value="gosend" type="radio" class="custom-control-input" required="">
              <label class="custom-control-label" for="gosend">GoSend</label>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-3 text text-muted">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
          <span class="text-muted">Shopping Cart</span>
          <span class="badge badge-secondary badge-pill"><?php echo count($products) ?> products</span>
        </h4>
        <ul class="list-group mb-3">
          <?php foreach($products as $product): ?>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div class="media">
                <div class="media-body">
                  <h5 class="my-0">
                    <?php echo $product["name"];?>
                  </h5>
                  <span class="text-muted">
                    (<?php echo $product["quantity"];?> items)
                  </span>
                </div>
              </div>
              <span class="text-muted">
                Rp <?php echo number_format((float)$product["subtotal"]);?>
              </span>
            </li>
          <?php endforeach; ?>
          <li class="list-group-item d-flex justify-content-between">
            <span>Total</span>
            <strong>Rp <?php echo number_format($total);?></strong>
          </li>
        </ul>

        <?php if (count($products) > 0): ?>
        <div class="card p-2">
          <div class="input-group">
            <button type="submit" class="btn btn-primary btn-block">Checkout</button>
          </div>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</form>