<?php $attrs = json_decode($data["attributes"]); ?>

<div class="row px-4">
  <div class="col-md-4 mb-3 text text-muted">
    <div class="border bg-white p-2">
      <img class="img-fluid" src="<?php echo url($attrs->image); ?>" width="100%" />
    </div>
  </div>

  <div class="border bg-white p-3 col-md-8">
    <h5 class="pb-3 mb-3 border-bottom text-muted">
      <?php echo $data["name"]; ?>
    </h5>
    <h2>
      <?php echo money($data["price"]); ?>
    </h2>

    <div class="form-inline mt-4">
      <!-- <input class="form-control input-lg m-1 text-right" type="number" value="1" style="width: 150px" /> -->
      <?php if ($data["stock"] <= 0): ?>
      <span>Stok Kosong</span>
      <?php else: ?>
      <a href="<?php echo url("cart/add/" . $data["id"]);?>" class="btn btn-primary m-1">
        <i class="fas fa-cart-plus"></i> <small>Tambah ke Keranjang</small>
      </a>
      <?php endif; ?>
      <!-- <button class="btn btn-danger m-1">
        <i class="fas fa-shopping-cart"></i> <small>Checkout</small>
      </button> -->
    </div>

    <div class="detail border-top mt-4 p-2 text">
      <?php echo $data["description"]; ?>
    </div>
  </div>
</div>