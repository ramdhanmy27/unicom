<!-- Sidebar -->
<div class="row">
  <div class="col-3 d-none d-md-block">
    <div id="menu" class="border bg-white p-2">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a
            href="<?php echo url("product"); ?>"
            class="nav-link text-truncate <?php echo !isset($category) ? "active" : "" ?>"
          >
            <small>All</small>
          </a>
        </li>

        <?php $menu_categories = App::db()->select("SELECT * FROM category order by name"); ?>
        <?php foreach ($menu_categories as $menu_category): ?>
          <li class="nav-item">
            <a
              href="<?php echo category_router($menu_category); ?>"
              class="nav-link text-truncate <?php echo $menu_category["id"] == $category["id"] ? "active" : "" ?>"
            >
              <small><?php echo $menu_category["name"]; ?></small>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>

  <!-- Content -->
  <div class="col-md-9 col-12 pb-4">
    <?php if ($data): ?>
      <div class="row bg-white border p-2">
        <?php foreach ($data as $product): ?>
          <?php $attrs = json_decode($product["attributes"]); ?>

          <div class="product col-3 p-0 card">
            <div class="card-body p-2">
              <div class="position-relative">
                <a href="<?php echo product_router($product); ?>">
                  <div class="img-container">
                    <div class="img-canvas">
                      <img class="card-img-top" src="<?php echo url($attrs->image); ?>">
                    </div>
                  </div>
                </a>

                <?php if ($product["stock"] > 0): ?>
                  <div class="cart-btn position-absolute">
                    <a href="<?php echo url("cart/add/" . $product["id"]); ?>" class="btn btn-primary">
                      <i class="fas fa-cart-plus"></i>
                      <small>Tambah</small>
                    </a>
                  </div>

                  <?php else: ?>

                  <div class="cart-btn position-absolute">
                    <button class="btn btn-default">
                      <small>Stok Kosong</small>
                    </button>
                  </div>
                  <?php endif; ?>
              </div>

              <div class="card-subtitle mt-1">
                <a class="product-name" href="<?php echo product_router($product); ?>">
                  <small><?php echo $product["name"]; ?></small>
                </a>
              </div>
              <div class="cart-text">
                <h5 class="text-danger text-truncate">
                  <?php echo money($product["price"]); ?>
                </h5>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    <?php else: ?>
      <div class="text-center text-muted mt-5">
        <h1><i class="fa fa-box"></i></h1>
        <h3>Product not found.</h3>
      </div>
    <?php endif; ?>
  </div>
</div>