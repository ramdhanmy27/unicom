<div class="col-md-10 offset-md-1 col-12 pb-4 mt-3">
  <h3>Popular</h3>

  <div class="row bg-white border p-2">
    <?php foreach ($popular as $product): ?>
      <?php $attrs = json_decode($product["attributes"]); ?>

      <div class="product col-3 p-0 card">
        <div class="card-body p-2">
          <div class="position-relative">
            <a href="<?php echo product_router($product); ?>">
              <div class="img-container">
                <div class="img-canvas">
                  <img class="card-img-top" src="<?php echo url($attrs->image); ?>">
                </div>
              </div>
            </a>

            <?php if ($product["stock"] > 0): ?>
            <div class="cart-btn position-absolute">
              <a href="<?php echo url("cart/add/" . $product["id"]); ?>" class="btn btn-primary">
                <i class="fas fa-cart-plus"></i>
                <small>Tambah</small>
              </a>
            </div>

            <?php else: ?>

            <div class="cart-btn position-absolute">
              <button class="btn btn-default">
                <small>Stok Kosong</small>
              </button>
            </div>
            <?php endif; ?>
          </div>

          <div class="card-subtitle mt-1">
            <a class="product-name" href="<?php echo product_router($product); ?>">
              <small><?php echo $product["name"]; ?></small>
            </a>
          </div>
          <div class="cart-text">
            <h5 class="text-danger text-truncate">
              <?php echo money($product["price"]); ?>
            </h5>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>

<div class="col-10 offset-1 d-none d-md-block">
  <h3>Kategori</h3>

  <div id="menu" class="row border bg-white p-2">

    <?php $menu_categories = App::db()->select("SELECT * FROM category order by name"); ?>

    <ul class="nav">
      <?php foreach ($menu_categories as $menu_category): ?>
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link text-truncate"
               href="<?php echo category_router($menu_category); ?>">
              <?php echo $menu_category["name"]; ?>
            </a>
          </li>
        </ul>
      <?php endforeach; ?>
    </ul>
  </div>
</div>

<br>
<br>