<h1 class="text-center mt-5 text-danger">
    <?php echo $code; ?>
</h1>

<h3 class="text-center text-muted">
    <?php echo $message; ?>
</h3>

<div class="text-center my-5">
    <a href="<?php echo url("/"); ?>" class="btn btn-primary">
        <i class="fa fa-home"></i> Home
    </a>
    <?php if (isset($_SERVER["HTTP_REFERER"])): ?>
        <a href="<?php echo $_SERVER["HTTP_REFERER"]; ?>" class="btn btn-info">
            <i class="fa fa-reply"></i> Back
        </a>
    <?php endif; ?>
</div>

<?php if (isset($backtrace)): ?>
    <hr size="1" />
    <div class="text-muted">
        <?php echo $backtrace; ?>
    </div>
<?php endif; ?>