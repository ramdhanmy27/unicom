<div class="col-3 d-none d-md-block">
  <div id="menu" class="border bg-white p-2">
    <ul class="nav nav-pills flex-column">
      <li class="nav-item">
        <a
          href="<?php echo url("auth/profile"); ?>"
          class="nav-link text-truncate <?php echo $path == "auth/profile" ? "active" : "" ?>"
        >
          <i class="fa fa-user"></i>
          <small>Profile</small>
        </a>
      </li>
      <li class="nav-item">
        <a
          href="<?php echo url("auth/account"); ?>"
          class="nav-link text-truncate <?php echo $path == "auth/account" ? "active" : "" ?>"
        >
          <i class="fa fa-user-secret"></i>
          <small>Account</small>
        </a>
      </li>
      <?php if (App::auth()->isStaff()): ?>
        <li class="nav-item">
          <a href="<?php echo url("admin/index"); ?>" class="nav-link text-truncate">
            <i class="fa fa-lock"></i>
            <small>Administrator</small>
          </a>
        </li>
      <?php endif; ?>
    </ul>
  </div>
</div>
