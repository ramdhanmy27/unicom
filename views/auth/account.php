<!-- Sidebar -->
<?php $path = App::request()->path(); ?>

<div class="row">
  <?php include "views/auth/sidebar.php"; ?>

  <!-- Content -->
  <div class="col-md-9 col-12 pb-4">
    <div class="card">
      <div class="card-header bg-white">
        <b>Data Account</b>
      </div>

      <form method="post">
        <div class="card-body pa">
          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right"> *Email </label>
            <div class="col-md-6">
              <input type="email" class="form-control" name="email" value="<?php echo $user["email"]; ?>" required />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right"> *Password Lama </label>
            <div class="col-md-6">
              <input type="password" class="form-control" name="password_old" required />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right"> Password Baru </label>
            <div class="col-md-6">
              <input type="password" class="form-control" name="password" />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right"> Konfirmasi Password </label>
            <div class="col-md-6">
              <input type="password" class="form-control" name="password_confirm" />
            </div>
          </div>

          <div class="form-group row">
            <div class="offset-md-4 col-md-6">
              <button type="submit" class="btn btn-primary">
                Register
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
