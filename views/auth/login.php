<div class="row justify-content-md-center mt-5">
  <div class="col col-md-5">
    <form method="post">
      <div class="card">
        <div class="card-header h3 text-white bg-dark">
          <i class="fa fa-lock"></i> Sign In
        </div>

        <div class="card-body">
          <div class="form-group">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fa fa-user"></i>
                </span>
              </div>
              <input type="email" class="form-control" name="email" placeholder="Email" required />
            </div>
          </div>

          <div class="form-group">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fa fa-lock"></i>
                </span>
              </div>
              <input type="password" class="form-control" name="password" placeholder="Password" required />
            </div>
          </div>

          <div class="form-group text-right">
            <button type="submit" class="btn btn-dark">
              login
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>