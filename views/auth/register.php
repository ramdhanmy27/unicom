<div class="row justify-content-md-center mt-5">
  <div class="col col-md-6">
    <form method="post">
      <div class="card">
        <div class="card-header h3 text-white bg-dark">
          <i class="fa fa-lock"></i> Register
        </div>

        <div class="card-body pa">
          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              Nama Lengkap
            </label>
            <div class="col-md-6">
              <input type="text" class="form-control" name="name" required />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              Jenis Kelamin
            </label>
            <div class="col-md-6">
              <label class="col-form-label mr-2">
                <input type="radio" name="gender" value="L" required checked="checked" /> Pria
              </label>
              <label class="col-form-label mr-2">
                <input type="radio" name="gender" value="P" required /> Wanita
              </label>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              Tanggal Lahir
            </label>
            <div class="col-md-6">
              <input type="date" class="form-control" name="date_birth" value="<?php echo $user["date_birth"]; ?>" required />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              Email
            </label>
            <div class="col-md-6">
              <input type="email" class="form-control" name="email" required />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              Password
            </label>
            <div class="col-md-6">
              <input type="password" class="form-control" name="password" required />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              Konfirmasi Password
            </label>
            <div class="col-md-6">
              <input type="password" class="form-control" name="password_confirm" required />
            </div>
          </div>

          <div class="form-group row">
            <div class="offset-md-4 col-md-6">
              <button type="submit" class="btn btn-dark">
                Register
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>