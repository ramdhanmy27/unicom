<!-- Sidebar -->
<?php $path = App::request()->path(); ?>

<div class="row">
  <?php include "views/auth/sidebar.php"; ?>

  <!-- Content -->
  <div class="col-md-9 col-12 pb-4">
    <div class="card">
      <div class="card-header bg-white">
        <b>Data Profile</b>
      </div>

      <form method="post">
        <div class="card-body pa">
          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              Nama Lengkap
            </label>
            <div class="col-md-6">
              <input type="text" class="form-control" name="name" value="<?php echo $user["name"]; ?>" required />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              Jenis Kelamin
            </label>
            <div class="col-md-6">
              <label class="col-form-label mr-2">
                <input type="radio" name="gender" value="L" required checked="checked" /> Pria
              </label>
              <label class="col-form-label mr-2">
                <input type="radio" name="gender" value="P" required /> Wanita
              </label>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              Tanggal Lahir
            </label>
            <div class="col-md-6">
              <input type="date" class="form-control" name="date_birth" value="<?php echo $user["date_birth"]; ?>" required />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label text-right">
              No. Telp / HP
            </label>
            <div class="col-md-6">
              <input type="number" class="form-control" name="phone" value="<?php echo $user["phone"]; ?>" required />
            </div>
          </div>

          <div class="form-group row">
            <div class="offset-md-4 col-md-6">
              <button type="submit" class="btn btn-primary">
                Register
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
