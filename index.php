<?php

require "env.php";

date_default_timezone_set('Asia/Jakarta');
error_reporting(DEBUG ? E_ALL : 0);

require 'lib/App.php';
App::init();
