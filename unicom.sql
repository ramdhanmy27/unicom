/*
Navicat MySQL Data Transfer

Source Server         : Localhost 5.7
Source Server Version : 50720
Source Host           : localhost:3307
Source Database       : unicom

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-07-12 01:56:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for addresses
-- ----------------------------
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `province` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `address` text NOT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `addresses_user_id` (`user_id`) USING BTREE,
  CONSTRAINT `addresses_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of addresses
-- ----------------------------

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  CONSTRAINT `category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'Komputer', null);
INSERT INTO `category` VALUES ('2', 'Televisi', null);
INSERT INTO `category` VALUES ('3', 'Handphone', null);
INSERT INTO `category` VALUES ('4', 'Baju', null);
INSERT INTO `category` VALUES ('5', 'Celana', null);
INSERT INTO `category` VALUES ('6', 'Jaket', null);
INSERT INTO `category` VALUES ('7', 'Meja', null);
INSERT INTO `category` VALUES ('8', 'Kursi', null);
INSERT INTO `category` VALUES ('9', 'Lemari', null);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `order_time` datetime NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `province` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `address` text NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  `carrier` varchar(5) DEFAULT NULL,
  `total` decimal(10,2) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `orders_user_id` (`user_id`) USING BTREE,
  CONSTRAINT `orders_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('00000000001', '1', '2018-07-12 01:11:50', 'Administrator', '089601195791', 'Jawa Barat - Kota Bandung', 'Cimahi Tengah', '40523', 'jalan simpang gang kapten ishak no 10 b', '2', 'dhl', '123123.00');
INSERT INTO `orders` VALUES ('00000000002', '1', '2018-07-12 01:51:23', 'Administrator', '', '', '', '', '', '0', '', '0.00');

-- ----------------------------
-- Table structure for order_confirmation
-- ----------------------------
DROP TABLE IF EXISTS `order_confirmation`;
CREATE TABLE `order_confirmation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `order_id` int(10) unsigned zerofill NOT NULL,
  `confirm_time` datetime NOT NULL,
  `note` text,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_confirmation_order_id` (`order_id`) USING BTREE,
  KEY `order_confirmation_user_id` (`user_id`) USING BTREE,
  CONSTRAINT `order_confirmation_order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_confirmation_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of order_confirmation
-- ----------------------------
INSERT INTO `order_confirmation` VALUES ('1', '1', '0000000001', '2018-07-12 01:47:52', null);

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item` (
  `order_id` int(11) unsigned zerofill NOT NULL,
  `product_id` int(11) unsigned zerofill NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `shipping_cost` decimal(10,2) unsigned NOT NULL,
  `total` decimal(10,2) unsigned NOT NULL,
  KEY `order_item_order_id` (`order_id`) USING BTREE,
  KEY `order_item_product_id` (`product_id`) USING BTREE,
  CONSTRAINT `order_item_order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_item_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of order_item
-- ----------------------------
INSERT INTO `order_item` VALUES ('00000000001', '00000000006', '1', '123123.00', '0.00', '123123.00');
INSERT INTO `order_item` VALUES ('00000000002', '00000000006', '2', '246246.00', '0.00', '0.00');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `attributes` json DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `category_id` int(11) unsigned NOT NULL,
  `stock` int(255) unsigned NOT NULL DEFAULT '0',
  `weight` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_category_id` (`category_id`) USING BTREE,
  CONSTRAINT `product_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('00000000006', 'asdfsdf', 'asdasd', '{\"image\": \"assets/img/products/1807125b4645f9b55a3.jpg\"}', '123123.00', '1212.00', '5', '1231', '12');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_staff` tinyint(1) DEFAULT '0',
  `date_birth` date DEFAULT NULL,
  `gender` enum('L','P') NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin@app.com', 'b6de6c44c85378caedcc11f9651fdab0', '1', null, 'L', null, 'Administrator');
