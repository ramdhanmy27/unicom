<?php

class ProductController
{
    public function index() {
        $data = $this->getDataProduct();

        return view("product/list", [
            "data" => $data,
        ]);
    }

    public function category($category_url) {
        $category_id = substr($category_url, strrpos($category_url, "-") + 1);

        $data = $this->getDataProduct($category_id);
        $category = App::db()->find("category", $category_id);

        if (!$category) {
            return abort(404);
        }

        return view("product/list", [
            "category" => $category,
            "data" => $data,
        ]);
    }

    private function getDataProduct($category_id = null) {
        $params = [];

        // filter by category
        if ($category_id !== null) {
            $params["category_id"] = $category_id;
        }

        // filter by keyword
        if (App::request()->has("q")) {
            $query = strtolower(App::request()->input("q"));
            $params["lower(name)"] = ["like", "%$query%"];
        }

        return App::db()->findAll("product", $params);
    }

    public function show($product_url) {
        $product_id = substr($product_url, strrpos($product_url, "-") + 1);
        $data = App::db()->find("product", $product_id);

        if (!$data) {
            return abort(404);
        }

        return view("product/show", [
            "data" => $data,
        ]);
    }
}
