<?php

class AuthController
{
    public function login() {
        redirect_if_user();
        $request = App::request();

        if ($request->isPost()) {
            $email = $request->input("email");
            $password = $request->input("password");

            App::auth()->signin($email, $password);

            // Login Success
            if (App::auth()->check()) {
                return redirect("/");
            }
            // Login Failed
            else {
                App::session()->flash("Login Failed", "danger");
                return redirect("auth/login");
            }
        }

        return view("auth/login");
    }

    public function logout() {
        redirect_if_guest();
        App::auth()->logout();

        return redirect("/");
    }

    public function register() {
        redirect_if_user();
        $request = App::request();

        if ($request->isPost()) {
            $this->validatePassword($request);

            // Create User
            $fields = [ "name", "email", "date_birth", "password", "gender", ];
            $data = [];

            foreach ($request->only($fields) as $field => $value) {
                switch ($field) {
                    case "name":
                        if (preg_match("/[\d\W]/", $value)) {
                            App::session()->flash("Nama yang anda masukkan tidak valid.", "success");
                            return back();
                        }

                    default: $data[$field] = $value; break;
                }
            }

            App::auth()->register($data);
            App::session()->flash("Akun anda berhasil terdaftar. Silahkan login untuk masuk.", "success");

            return redirect("auth/login");
        }

        return view("auth/register");
    }

    public function profile() {
        redirect_if_guest();
        $user = App::auth()->user();
        $request = App::request();

        if ($request->isPost()) {
            // Update User Profile
            $fields = [ "name", "date_birth", "phone", "gender", ];
            $data = [];

            foreach ($request->only($fields) as $field => $value) {
                switch ($field) {
                    case "name":
                        if (preg_match("/[\d\W]/", $value)) {
                            App::session()->flash("Nama yang anda masukkan tidak valid.", "success");
                            return back();
                        }

                    default: $data[$field] = $value; break;
                }
            }

            App::db()->update("users", $data, $user["id"]);
            App::session()->flash("Data profile anda berhasil diperbarui.", "success");

            return back();
        }

        return view("auth/profile", [
            "user" => $user,
        ]);
    }

    public function account() {
        redirect_if_guest();
        $user = App::auth()->user();
        $request = App::request();

        if ($request->isPost()) {
            if (md5($request->password_old) != $user["password"]) {
                App::session()->flash("Password anda salah.", "danger");
                return back();
            }

            $update = ["email" => $request->email];

            if ($request->password) {
                $this->validatePassword($request);
                $update["password"] = md5($request->password);
            }

            App::db()->update("users", $update, $user["id"]);
            App::session()->flash("Akun anda berhasil diperbarui.", "success");

            return back();
        }

        return view("auth/account", [
            "user" => $user,
        ]);
    }

    private function validatePassword($request) {
        if (strlen($request->password) < 6) {
            App::session()->flash("Ukuran password minimal 6 karakter", "danger");
            return back();
        }

        // Validation
        if ($request->password != $request->password_confirm) {
            App::session()->flash("Konfirmasi Password Tidak Valid.", "danger");
            return back();
        }
    }
}
