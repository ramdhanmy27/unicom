<?php

class IndexController
{
    public function index() {
        $popular = App::db()->select("SELECT * FROM product limit 4");

        return view("home", [
            "popular" => $popular,
        ]);
    }
}
