<?php

class CartController
{
    public function index() {
        redirect_if_guest();
        $user = App::auth()->user();

        $sql = "SELECT * FROM orders WHERE user_id = ? AND status = ?";
        $order = App::db()->query($sql, [$user["id"], 0])->fetch();

        $sql = "SELECT * FROM order_item AS oi
                JOIN product AS p ON p.id = oi.product_id
                WHERE oi.order_id = ?";

        $products = App::db()->query($sql, [$order["id"]])->fetchAll();

        $total = array_sum(array_map(function($product) { return $product["subtotal"]; }, $products));

        return view("product/cart", [
            "user" => $user,
            "order" => $order,
            "products" => $products,
            "total" => $total,
        ]);
    }

    public function add($id) {
        if (redirect_if_guest()) {
            return;
        }

        $product = App::db()->find("product", $id);
        $user = App::auth()->user();

        if ($product["stock"] <= 0)
            App::session()->flash("Stock is empty!", "info");

        if (empty($user) || !isset($user) || $product["stock"] <= 0)
            // redirect to previous page
            return back();

        App::db()->beginTransaction();
        try {
            $address = App::db()->find("addresses", ["user_id" => $user["id"]]);

            $sql = "SELECT * FROM orders WHERE user_id = ? AND status = ?";
            $order = App::db()->query($sql, [$user["id"], 0])->fetch();

            $id = null;
            if (!$order) {
                $order = [
                    "user_id"     => $user["id"],
                    "order_time"  => date('Y-m-d H:i:s'),
                    "name"        => $user["name"],
                    "phone"       => $user["phone"] ? $user["phone"] : "",
                    "province"    => "",
                    "city"        => "",
                    "postal_code" => "",
                    "address"     => "",
                    "status"      => 0,
                    "carrier"     => "",
                    "total"       => 0,
                ];

                if ($address) {
                    $order["province"]    = $address["province"];
                    $order["city"]        = $address["city"];
                    $order["postal_code"] = $address["postal_code"];
                    $order["address"]     = $address["address"];
                }

                $pdo = App::db()->insert("orders", $order);
                $order["id"] = App::db()->lastInsertId("id");
            }

            $qty = 0;

            $sql = "SELECT COUNT(*) AS qty FROM order_item WHERE order_id = ? AND product_id = ?";
            $items = App::db()->query($sql, [$order["id"], $product["id"]])->fetch();

            if ($items)
                $qty = $items["qty"];

            // add new item to cart
            $order_item = [
                "order_id"      => $order["id"],
                "product_id"    => $product["id"],
                "quantity"      => $qty + 1,
                "subtotal"      => $product["price"] * ($qty + 1),
                "shipping_cost" => 0,
                "total"         => 0,
            ];

            if ($qty > 0) {
                $sql = "UPDATE order_item
                        SET quantity = ?,
                            subtotal = ?
                        WHERE order_id = ? AND product_id = ?";

                // add qty
                App::db()->query($sql, [$qty + 1, $order_item["subtotal"], $order["id"], $product["id"]]);
            } else {
                // new item
                App::db()->insert("order_item", $order_item);
            }

            App::db()->commit();
            App::session()->flash("'". $product["name"] ."' added to cart!", "success");
        } catch(\Exception $e) {
            App::db()->rollBack();
            App::session()->flash("Something happened! Failed to add product to cart!", "danger");
        }

        // redirect to previous page
        return back();
    }

    public function checkout() {
        redirect_if_guest();
        $user = App::auth()->user();
        $request = App::request();

        $input = [
            "name"        => $request->input("name"),
            "phone"       => $request->input("phone"),
            "address"     => $request->input("address"),
            "city"        => $request->input("city"),
            "province"    => $request->input("province"),
            "postal_code" => $request->input("postal_code"),
            "carrier"     => $request->input("carrier"),
        ];

        $labels = [
            "name"        => "Name",
            "phone"       => "Phone",
            "address"     => "Address",
            "city"        => "City",
            "province"    => "Province",
            "postal_code" => "Postal Code",
            "carrier"     => "Carrier",
        ];

        foreach ($input as $key => $val) {
            if (empty($val) || !isset($val)) {
                App::session()->flash($labels[$key] . " is required!", "danger");

                return redirect("cart");
            }
        }

        App::db()->beginTransaction();
        try {
            $sql = "SELECT * FROM orders WHERE user_id = ? AND status = ?";
            $order = App::db()->query($sql, [$user["id"], 0])->fetch();

            $sql = "SELECT SUM(subtotal) AS total FROM order_item WHERE order_id = ?";
            $fetch = App::db()->query($sql, [$order["id"]])->fetch();
            $total = $fetch["total"];

            // update total on items
            $sql = "UPDATE order_item SET total = ? WHERE order_id = ?";
            App::db()->query($sql, [$total, $order["id"]]);

            // update order
            $sql = "UPDATE orders
                    SET name = ?, phone = ?, address = ?, city = ?, province = ?,
                        postal_code = ?, carrier = ?, status = 1, total = ?
                    WHERE id = ?";

            $input = array_values($input);
            $input[] = $total;
            $input[] = $order["id"];

            App::db()->query($sql, $input);

            App::db()->commit();

            App::session()->flash("checkout success!", "success");
        } catch(\Exception $e) {
            App::db()->rollBack();

            App::session()->flash("checkout failed!", "danger");
        }

        return back();
    }
}
