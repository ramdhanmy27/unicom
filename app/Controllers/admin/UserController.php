<?php

class UserController
{
    private $fields = [
        "name" => "Nama",
        "email" => "Email",
        "status" => "Status",
        "date_birth" => "Tanggal Lahir",
        "gender" => "Jenis Kelamin",
        "phone" => "Telepon",
    ];

    public function index() {
        $results = App::db()->select(
            "SELECT u.* FROM users u"
        );

        $results = array_map(function($item) {
            $item["status"] = $item["is_staff"] ? "Staff" : "User";
            $item["gender"] = $item["gender"] == "L" ? "Laki-laki" : "Perempuan";

            return $item;
        }, $results);

        return view('admin/user/list', [
            "fields" => $this->fields,
            "results" => $results,
        ]);
    }

    public function create() {
        if (App::request()->isPost()) {
            $insert = [];
            $columns = [];
            $fields = [
                "name", "email", "is_staff",
                "date_birth", "gender", "phone",
            ];


            foreach ($fields as $field) {
                $value = App::request()->input($field);

                if ($value === null) {
                    continue;
                }

                $columns[] = "`$field`";
                $insert[] = "'$value'";
            }

            $columns[] = "`is_staff`";
            $insert[] = 1;

            $password = App::request()->input("password");
            if (!empty($password)) {
                $columns[] = "`password`";
                $insert[] = "'". App::auth()->hash($password) ."'";
            }

            $sql = "INSERT INTO users (".implode(",", $columns).") VALUES (".implode(",", $insert).")";
            App::db()->query($sql);

            redirect("admin/user");
        }

        return view('admin/user/create', [
            "fields" => $this->fields,
        ]);
    }

    public function update($id) {
        // fetch data by id
        $results = App::db()->select("SELECT * FROM users where id=?", [$id]);

        if (count($results) === 0) {
            redirect();
        }

        $data = current($results);

        if (!$data["is_staff"])
            redirect("admin/user");

        if (count($_POST) > 0) {
            $update_fields = [];

            $fields = [
                "name", "email", "is_staff",
                "date_birth", "gender", "phone",
            ];

            foreach ($fields as $field) {
                $value = App::request()->input($field);

                if ($value === null) {
                    continue;
                }

                $update_fields[] = "`$field`='$value'";
            }

            $password = App::request()->input("password");
            if (!empty($password)) {
                $update_fields[] = "`password`=" . "'". App::auth()->hash($password) ."'";
            }

            App::db()->query(
                "UPDATE users SET ".implode(",", $update_fields)." where id=?", [$id]
            );

            redirect("admin/user");
        }
        else {
            $this->fields["password"] = "Password";

            return view('admin/user/update', [
                "fields" => $this->fields,
                "data" => $data,
            ]);
        }
    }

    public function delete($id) {
        $user = App::db()->find("users", $id);

        if (!$user) {
            return abort(404);
        }

        App::db()->query("DELETE FROM users where id=?", [$id]);
        redirect("admin/user");
    }
}
