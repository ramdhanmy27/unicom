<?php

class OrdersController
{
	function index() {
		$pdo = App::db()->query("SELECT * FROM orders o where status!='0' order by order_time desc");

        return view("admin/orders/list", [
            "data" => $pdo->fetchAll(PDO::FETCH_ASSOC),
        ]);
	}

	function sent($id) {
		$order = App::db()->find("orders", $id);

        if (!$order) {
            return abort(404);
        }

        // Create Confirm Order
        App::db()->insert("order_confirmation", [
        	"user_id" => $order["user_id"],
        	"order_id" => $order["id"],
        	"confirm_time" => date("Y-m-d H:i:s"),
    	]);

        // Update order status
        App::db()->update("orders", ["status" => "2"], $id);

        return back();
	}
}
