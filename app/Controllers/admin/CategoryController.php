<?php

class CategoryController
{
    private $fields = [
        "name" => "Name",
        "parent_id" => "Parent",
    ];

    public function index() {
        $results = App::db()->select(
            "SELECT c.*, p.name as parent_id FROM category c
            left join category p on p.id=c.parent_id"
        );

        return view('admin/category/list', [
            "fields" => $this->fields,
            "results" => $results,
        ]);
    }

    public function create() {
        if (App::request()->isPost()) {
            $insert = [];
            $columns = [];

            foreach ($this->fields as $field => $label) {
                $value = App::request()->input($field);

                if ($value === null) {
                    continue;
                }

                $columns[] = "`$field`";
                $insert[] = "'$value'";
            }

            $sql = "INSERT INTO category (".implode(",", $columns).") VALUES (".implode(",", $insert).")";
            App::db()->query($sql);

            redirect("admin/category");
        }

        return view('admin/category/create', [
            "fields" => $this->fields,
        ]);
    }

    public function update($id) {
        // fetch data by id
        $results = App::db()->select("SELECT * FROM category where id=?", [$id]);

        if (count($results) === 0) {
            redirect();
        }

        $data = current($results);

        if (count($_POST) > 0) {
            $update_fields = [];

            foreach ($this->fields as $field => $label) {
                $value = App::request()->input($field);

                if ($value === null) {
                    continue;
                }

                $update_fields[] = "`$field`='$value'";
            }

            App::db()->query(
                "UPDATE category SET ".implode(",", $update_fields)." where id=?", [$id]
            );

            redirect("admin/category");
        }
        else {
            return view('admin/category/update', [
                "fields" => $this->fields,
                "data" => $data,
            ]);
        }
    }

    public function delete($id) {
        $category = App::db()->find("category", $id);

        if (!$category) {
            return abort(404);
        }

        App::db()->query("DELETE FROM category where id=?", [$id]);
        redirect("admin/category");
    }
}
