<?php

class ProductController
{
    private $fields = [
        "name" => "Name",
        "description" => "Description",
        "attributes" => "Attributes",
        "price" => "Price",
        "discount" => "Discount",
        "category_id" => "Category",
        "stock" => "Stock",
        "weight" => "Weight",
    ];

    public function index() {
        $results = App::db()->select(
            "SELECT p.*, c.name as category_id FROM product p
            INNER JOIN category c on c.id=p.category_id
            order by p.id desc"
        );

        return view('admin/product/list', [
            "fields" => $this->fields,
            "results" => $results,
        ]);
    }

    public function create() {
        if (App::request()->isPost()) {
            $insert = [];
            $columns = [];

            foreach ($this->fields as $field => $label) {
                $value = App::request()->input($field);

                if ($value === null || $value === "") {
                    continue;
                }

                switch ($field) {
                    case "name": $value = strip_tags($field); break;
                    case "price":
                    case "discount":
                    case "stock":
                    case "weight":
                        if ($value < 0) {
                            App::session()->flash("Nilai tidak boleh negatif!", "danger");
                            return back();
                        }
                        break;
                }

                $columns[] = "`$field`";
                $insert[] = "'$value'";
            }

            // upload image
            if (App::request()->hasFile("img")) {
                $file = App::request()->file("img");

                if (!has_extension($file["name"], ["png", "jpg", "jpeg"])) {
                    App::session()->flash("File gambar yang diupload tidak valid!", "danger");
                    return back();
                }

                $img_path = "assets/img/products/".uniqid(date("ymd")).".jpg";
                move_uploaded_file($file["tmp_name"], $img_path);

                $columns[] = "`attributes`";
                $insert[] = "'".json_encode(["image" => $img_path])."'";
            }

            $sql = "INSERT INTO product (".implode(",", $columns).") VALUES (".implode(",", $insert).")";
            App::db()->query($sql);

            redirect("admin/product");
        }

        return view('admin/product/create', [
            "fields" => $this->fields,
        ]);
    }

    public function update($id) {
        // fetch data by id
        $results = App::db()->select("SELECT * FROM product where id=?", [$id]);

        if (count($results) === 0) {
            redirect();
        }

        $data = current($results);

        if (count($_POST) > 0) {
            $update_fields = [];

            foreach ($this->fields as $field => $label) {
                $value = App::request()->input($field);

                if ($value === null) {
                    continue;
                }

                switch ($field) {
                    case "name": $value = strip_tags($field); break;
                    case "price":
                    case "discount":
                    case "stock":
                    case "weight":
                        if ($value < 0) {
                            App::session()->flash("Nilai tidak boleh negatif!", "danger");
                            return back();
                        }
                        break;
                }

                $update_fields[] = "`$field`='$value'";
            }

            // upload image
            if (App::request()->hasFile("img")) {
                // remove old image
                if (isset($data["attributes"]) && ($attributes = json_decode($data["attributes"]))) {
                    if (file_exists($attributes->image)) {
                        @unlink($attributes->image);
                    }
                }

                $file = App::request()->file("img");

                if (!has_extension($file["name"], ["png", "jpg", "jpeg"])) {
                    App::session()->flash("File gambar yang diupload tidak valid!", "danger");
                    return back();
                }

                $img_path = "assets/img/products/".uniqid(date("ymd")).".png";
                move_uploaded_file($file["tmp_name"], "./$img_path");

                $update_fields[] = "`attributes`='".json_encode(["image" => $img_path])."'";
            }

            App::db()->query(
                "UPDATE product SET ".implode(",", $update_fields)." where id=?", [$id]
            );

            redirect("admin/product");
        }
        else {
            return view('admin/product/update', [
                "fields" => $this->fields,
                "data" => $data,
            ]);
        }
    }

    public function delete($id) {
        $product = App::db()->find("product", $id);

        if (!$product) {
            return abort(404);
        }

        App::db()->query("DELETE FROM product where id=?", [$id]);
        redirect("admin/product");
    }
}
