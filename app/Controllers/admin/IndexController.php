<?php

class IndexController
{
    public function index() {

        $sql = "SELECT * FROM (
            SELECT p.*, COUNT(o.quantity) AS count FROM order_item o
            JOIN product p ON p.id = o.product_id
            GROUP BY p.id
        ) t
        ORDER BY count DESC LIMIT 10";

        $top_products = App::db()->query($sql)->fetchAll();


        $sql = "SELECT COUNT(*) AS count FROM orders o
                WHERE MONTH(order_time) = ?";

        $sell_count = App::db()->query($sql, [date('m')])->fetch()["count"];

        return view("admin/index", [
            "message" => "Welcome Admin",
            "products" => $top_products,
            "sell_count" => $sell_count,
        ]);
    }
}
